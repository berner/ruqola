/*
   SPDX-FileCopyrightText: 2019-2023 Laurent Montel <montel@kde.org>

   SPDX-License-Identifier: LGPL-2.0-or-later
*/

#include "unicodeemoticonparsertest.h"
#include "emoticons/unicodeemoticonparser.h"
#include <QTest>

QTEST_GUILESS_MAIN(UnicodeEmoticonParserTest)

UnicodeEmoticonParserTest::UnicodeEmoticonParserTest(QObject *parent)
    : QObject(parent)
{
}

// TODO add test parser
