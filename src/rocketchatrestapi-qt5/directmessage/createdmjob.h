/*
   SPDX-FileCopyrightText: 2018-2023 Laurent Montel <montel@kde.org>

   SPDX-License-Identifier: LGPL-2.0-or-later
*/

#pragma once

#include "librestapi_private_export.h"
#include "restapiabstractjob.h"
namespace RocketChatRestApi
{
class LIBROCKETCHATRESTAPI_QT5_TESTS_EXPORT CreateDmJob : public RestApiAbstractJob
{
    Q_OBJECT
public:
    explicit CreateDmJob(QObject *parent = nullptr);
    ~CreateDmJob() override;

    Q_REQUIRED_RESULT bool start() override;
    Q_REQUIRED_RESULT bool requireHttpAuthentication() const override;
    Q_REQUIRED_RESULT bool canStart() const override;

    Q_REQUIRED_RESULT QNetworkRequest request() const override;

    Q_REQUIRED_RESULT QJsonDocument json() const;

    Q_REQUIRED_RESULT QStringList userNames() const;
    void setUserNames(const QStringList &userNames);

Q_SIGNALS:
    void createDmDone();

private:
    Q_DISABLE_COPY(CreateDmJob)
    void onPostRequestResponse(const QJsonDocument &replyJson) override;
    QStringList mUserNames;
};
}
