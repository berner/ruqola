/*
   SPDX-FileCopyrightText: 2018-2023 Laurent Montel <montel@kde.org>

   SPDX-License-Identifier: LGPL-2.0-or-later
*/

#pragma once

#include "librocketchatrestapi-qt5_export.h"
#include "userbasejob.h"

#include <QNetworkRequest>
namespace RocketChatRestApi
{
class LIBROCKETCHATRESTAPI_QT5_EXPORT UserInfoJob : public UserBaseJob
{
    Q_OBJECT
public:
    explicit UserInfoJob(QObject *parent = nullptr);
    ~UserInfoJob() override;

    Q_REQUIRED_RESULT bool requireHttpAuthentication() const override;

    Q_REQUIRED_RESULT bool start() override;

    Q_REQUIRED_RESULT bool canStart() const override;

    Q_REQUIRED_RESULT QNetworkRequest request() const override;
Q_SIGNALS:
    void userInfoDone(const QJsonObject &obj);

private:
    Q_DISABLE_COPY(UserInfoJob)
    void onGetRequestResponse(const QJsonDocument &replyJson) override;
};
}
